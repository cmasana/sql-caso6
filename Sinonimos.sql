/* 
 * SINÓNIMOS EN SQL
 * Descripción: ASIX-DAW1B UF2 RA2 CAS6
 * Autores: Vila, Pascual y Masana (Grupo 3)
 * Título: Viajes Montsià
 */
 
/* Sinónimos para las vistas */
CREATE SYNONYM cli_cab FOR view_cli_cab;
CREATE SYNONYM emp_act FOR view_emp_act;
CREATE SYNONYM emp_inc FOR view_emp_inc;
CREATE SYNONYM res_act FOR view_res_act;
CREATE SYNONYM res_exc FOR view_res_exc;
CREATE SYNONYM res_rest FOR view_res_rest;
CREATE SYNONYM v_rutas FOR view_rutas;
------------------------------------------
CREATE SYNONYM cli_latino FOR view_clientes_baile;
CREATE SYNONYM cli_sin_act FOR view_clientes_sin_actividades;
CREATE SYNONYM cli_con_act FOR view_clientes_con_actividades;
------------------------------------------
CREATE SYNONYM trab_cabina FOR view_cab_emp;
CREATE SYNONYM nomb_res FOR view_nombre_reserva;



/*
Sinónimos de tablas para otros usuarios. De esta manera no necesitamos introducir
el nombre del usuario propietario antes de consultar, modificar, insertar o eliminar
datos en una tabla 
(Este código tiene que ejecutarlo el usuario NO propietario)
*/
CREATE SYNONYM actividad FOR masana.actividad;
CREATE SYNONYM cabinas FOR masana.cabinas; 
CREATE SYNONYM checkin FOR masana.checkin; 
CREATE SYNONYM client FOR masana.client;
CREATE SYNONYM empleado FOR masana.empleado; 
CREATE SYNONYM empleado_actividad FOR masana.empleado_actividad; 
CREATE SYNONYM empleado_excursion FOR masana.empleado_excursion; 
CREATE SYNONYM empleado_restaurante FOR masana.empleado_restaurante;
CREATE SYNONYM empleado_tienda FOR masana.empleado_tienda;
CREATE SYNONYM excursion FOR masana.excursion;
CREATE SYNONYM incidencia FOR masana.incidencia;
CREATE SYNONYM lista_puertos FOR masana.lista_puertos;
CREATE SYNONYM puerto FOR masana.puerto;
CREATE SYNONYM reserva FOR masana.reserva;
CREATE SYNONYM reserva_actividad FOR masana.reserva_actividad;
CREATE SYNONYM reserva_excursion FOR masana.reserva_excursion;
CREATE SYNONYM reserva_restaurante FOR masana.reserva_restaurante;
CREATE SYNONYM restaurante FOR masana.restaurante;
CREATE SYNONYM ruta FOR masana.ruta;
CREATE SYNONYM ruta_empleado FOR masana.ruta_empleado;
CREATE SYNONYM tienda FOR masana.tienda;


/* Para eliminar un sinónimo */
-- DROP SYNONYM (nombre)
-- DROP SYNONYM actividad
-- DROP SYNONYM cabinas