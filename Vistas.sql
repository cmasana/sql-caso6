/* 
 * CREACIÓN DE VISTAS EN SQL
 * Descripción: ASIX-DAW1B UF2 RA2 CAS6
 * Autores: Vila, Pascual y Masana (Grupo 3)
 * Título: Viajes Montsià
 */

/* Muestra la lista de excursiones que hay asociadas a cada reserva */
CREATE OR REPLACE VIEW view_res_exc
AS SELECT reserva.id_reserva, reserva_excursion.fecha_hora, excursion.descripcion, excursion.edad_minima, excursion.precio 
FROM reserva, reserva_excursion, excursion
WHERE reserva.id_reserva = reserva_excursion.id_reserva
AND reserva_excursion.id_excursion = excursion.id_excursion;

/* Muestra una lista de excursiones asociadas a la reserva 1 */
SELECT fecha_hora, descripcion, edad_minima, precio FROM view_res_exc
WHERE id_reserva = '1';

-- select * from reserva;
-- select * from reserva_excursion;
-- select * from excursion;

/* Muestra la lista de actividades que hay asociadas a cada reserva */
CREATE OR REPLACE VIEW view_res_act
AS SELECT reserva.id_reserva, reserva_actividad.fecha_hora, actividad.nombre, actividad.descripcion, actividad.edad_minima 
FROM reserva, reserva_actividad, actividad
WHERE reserva.id_reserva = reserva_actividad.id_reserva
AND reserva_actividad.nombre = actividad.nombre;

/* Muestra la lista de actividades asociadas a la reserva 1 */
SELECT fecha_hora, nombre, descripcion, edad_minima FROM view_res_act
WHERE id_reserva = '1';


-- select * from reserva;
-- select * from reserva_actividad;
-- select * from actividad;

/* Muestra la lista de  restaurantes que hay asociados a cada reserva */
CREATE OR REPLACE VIEW view_res_rest
AS SELECT reserva.id_reserva, reserva_restaurante.fecha_hora_res, restaurante.nombre, restaurante.menu, restaurante.precio 
FROM reserva, reserva_restaurante, restaurante
WHERE reserva.id_reserva = reserva_restaurante.id_reserva
AND reserva_restaurante.nombre = restaurante.nombre;

/* Muestra la lista de restaurantes asociados a la reserva 1 */
SELECT fecha_hora_res, nombre, menu, precio FROM view_res_rest
WHERE id_reserva = '1';


-- select * from reserva;
-- select * from reserva_restaurante;
-- select * from restaurante;

/* Muestra la lista de incidencias que están asociadas a los empleados */
CREATE OR REPLACE VIEW view_emp_inc
AS SELECT empleado.dni, empleado.nss, incidencia.num_incidencia, incidencia.descripcion FROM empleado
INNER JOIN incidencia ON incidencia.nss = empleado.nss;

/* Comprobación */
SELECT * FROM view_emp_inc;


/* Muestra la lista de clientes que tienen asignada una cabina */
CREATE OR REPLACE VIEW view_cli_cab
AS SELECT cabinas.id_reserva, client.nombre, client.dni, cabinas.num_cabina
FROM cabinas, checkin, client
WHERE cabinas.id_reserva = checkin.id_reserva
AND checkin.dni = client.dni;

/* Lista de clientes asignados a la cabina nº 443 */
SELECT * FROM view_cli_cab
WHERE num_cabina = 443;


/* Muestra la lista de rutas de nuestro barco */
CREATE OR REPLACE VIEW view_rutas
AS SELECT puerto.id_puerto, puerto.localidad, lista_puertos.situacion, ruta.estacion
FROM puerto, lista_puertos, ruta
WHERE puerto.id_puerto = lista_puertos.id_puerto
AND ruta.estacion = lista_puertos.estacion;

/* Lista de los puertos en la ruta que se hace en verano */
SELECT * FROM view_rutas
WHERE estacion = 'verano';

-- select * from ruta;
-- select * from lista_puertos;
-- select * from puerto;

/* Muestra los empleados que están asignados a alguna actividad */
CREATE OR REPLACE VIEW view_emp_act
AS SELECT actividad.nombre, empleado_actividad.nss, empleado_actividad.horas_actividad
FROM actividad, empleado_actividad
WHERE actividad.nombre = empleado_actividad.nombre;

/* Comprobación */
SELECT * FROM view_emp_act;

-- select * from actividad;
-- select * from empleado_actividad;

/* Muestra una lista de todos los clientes que han contratado la actividad baile latino */
CREATE OR REPLACE VIEW view_clientes_baile
AS SELECT client.nombre, reserva.actividades_extra
FROM client, reserva
WHERE client.dni = reserva.dni
AND reserva.actividades_extra = 'baile latino';

/* Comprobación */
SELECT * FROM view_clientes_baile;

/* Muestra una lista de todos los clientes que no tienen una actividad contratada */
CREATE OR REPLACE VIEW view_clientes_sin_actividades
AS SELECT client.nombre, reserva.actividades_extra
FROM client, reserva
WHERE client.dni = reserva.dni
AND reserva.actividades_extra IS NULL;

/* Comprobación */
SELECT * FROM view_clientes_sin_actividades;

/* Muestra una lista de todos los clientes que si tienen una actividad contratada */
CREATE OR REPLACE VIEW view_clientes_con_actividades
AS SELECT client.nombre, reserva.actividades_extra
FROM client, reserva
WHERE client.dni = reserva.dni
AND reserva.actividades_extra IS NOT NULL;

/* Comprobación */
SELECT * FROM view_clientes_con_actividades;

/* Listado de la cabina que limpia cada empleado */ 
CREATE OR REPLACE VIEW view_cab_emp
AS SELECT cabinas.num_cabina, empleado.nss
FROM cabinas, empleado
WHERE cabinas.nss = empleado.nss;

/* Comprobación */
SELECT * FROM view_cab_emp;

/* Listado de todas las reservas junto al nombre del cliente y su DNI */
CREATE OR REPLACE VIEW view_nombre_reserva
AS SELECT reserva.id_reserva, reserva.dni, client.nombre
FROM client, reserva
WHERE reserva.dni = client.dni
ORDER BY reserva.id_reserva;

/* Comprobación */
SELECT * FROM view_nombre_reserva;



/* Para eliminar una vista utilizamos: */
-- DROP VIEW (nombre vista)
-- DROP VIEW view_cli_cab
-- DROP VIEW view_emp_act