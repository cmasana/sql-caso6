/* 
 * INSERCIÓN DE DATOS EN SQL
 * Descripción: ASIX-DAW1B UF2 RA2 CAS6
 * Autores: Vila, Pascual y Masana (Grupo 3)
 * Título: Viajes Montsià
 */

/* Insertar datos en la tabla cliente */
INSERT INTO client (dni, nombre, fecha_nacimiento)
VALUES ('48675465Z', 'Joan', TO_DATE('12-05-2000', 'dd-MM-yyyy'));

INSERT INTO client (dni, nombre, fecha_nacimiento)
VALUES ('41675445G', 'Alfonso', TO_DATE('12-02-1999', 'dd-MM-yyyy'));

INSERT INTO client (dni, nombre, fecha_nacimiento)
VALUES ('52675465Z', 'Teresa', TO_DATE('10-08-1980', 'dd-MM-yyyy'));

INSERT INTO client (dni, nombre, fecha_nacimiento)
VALUES ('32675465Z', 'Lorena', TO_DATE('12-03-1985', 'dd-MM-yyyy'));

INSERT INTO client (dni, nombre, fecha_nacimiento)
VALUES ('31133465V', 'Marcos', TO_DATE('15-09-1981', 'dd-MM-yyyy'));

/* Insertar datos en la tabla ruta */
INSERT INTO ruta (estacion)
VALUES ('verano');

INSERT INTO ruta
VALUES ('primavera');

INSERT INTO ruta
VALUES ('otoño');

INSERT INTO ruta
VALUES ('invierno');

/* Insertar datos en la tabla reserva */
INSERT INTO reserva (id_reserva, precios, horario_comedor_principal, excursiones_opcionales,
actividades_extra, dni, estacion, reg_alojamiento)
VALUES (1, 1000, '19:00', '1001', 'baile latino', '48675465Z', 'verano', 'PC');

INSERT INTO reserva
VALUES (2, 1500, '22:00', '2002', 'casino', '41675445G', 'invierno', 'MP');

INSERT INTO reserva
VALUES (3, 2000, null, '3003', 'yoga', '32675465Z', 'verano', 'SA');

INSERT INTO reserva
VALUES (4, 2000, null, null, null, '52675465Z', 'verano', 'SA');

/* Insertar datos en la tabla checkin */
INSERT INTO checkin (dni, id_reserva)
VALUES ('48675465Z', 1);

INSERT INTO checkin (dni, id_reserva)
VALUES ('31133465V', 4);

INSERT INTO checkin (dni, id_reserva)
VALUES ('52675465Z', 4);

/* Insertar datos en la tabla puerto */
INSERT INTO puerto (id_puerto, localidad)
VALUES ('FL2', 'Miami');

INSERT INTO puerto (id_puerto, localidad)
VALUES ('ES8', 'Barcelona');

INSERT INTO puerto (id_puerto, localidad)
VALUES ('CU1', 'La Habana');

/* Insertar datos en la tabla lista_puertos */
INSERT INTO lista_puertos (estacion, id_puerto, situacion)
VALUES ('verano', 'FL2', 'destino');

INSERT INTO lista_puertos (estacion, id_puerto, situacion)
VALUES ('verano', 'CU1', 'paso');

INSERT INTO lista_puertos (estacion, id_puerto, situacion)
VALUES ('verano', 'ES8', 'origen');

INSERT INTO lista_puertos (estacion, id_puerto, situacion)
VALUES ('invierno', 'CU1', 'destino');

INSERT INTO lista_puertos (estacion, id_puerto, situacion)
VALUES ('invierno', 'FL2', 'paso');

INSERT INTO lista_puertos (estacion, id_puerto, situacion)
VALUES ('invierno', 'ES8', 'origen');

/* Insertar datos en la tabla empleados */
INSERT INTO empleado (dni, nss, nss2, gerente, horario, lugar_de_trabajo, fecha_contratacion, fecha_finalizacion)
VALUES ('43388907L', '3200123456', null, 'Si', 'Mañana', 'Cabinas Suite',
TO_DATE ('12-02-1999', 'DD-MM-YYYY'), TO_DATE ('12-02-2021', 'DD-MM-YYYY'));

INSERT INTO empleado
VALUES ('35388904B', '3200234567', '3200123456', 'No', 'Tarde', 'Cabinas Exterior',
TO_DATE ('15-03-1998', 'DD-MM-YYYY'), null);

INSERT INTO empleado
VALUES ('43388912D', '3200456789', null, 'Si', 'Noche', 'Piscina Proa',
TO_DATE ('05-01-2008', 'DD-MM-YYYY'),  TO_DATE ('01-01-2020', 'DD-MM-YYYY'));

INSERT INTO empleado
VALUES ('42188925R', '4300234567', null, 'Si', 'Mañana', 'SPA',
TO_DATE ('30-04-2001', 'DD-MM-YYYY'), null);

INSERT INTO empleado
VALUES ('33458465E', '3200254599', null, 'Si', 'Mañana', 'Mantenimiento',
TO_DATE ('30-04-2001', 'DD-MM-YYYY'), null);

INSERT INTO empleado
VALUES ('44458963Y', '3200253486', null, 'Si', 'Mañana', 'Comedor Principal',
TO_DATE ('30-04-2001', 'DD-MM-YYYY'), null);

INSERT INTO empleado
VALUES ('45659325U', '3200345322', null, 'No', 'Noche', 'Estudio Yoga',
TO_DATE ('12-05-1979', 'DD-MM-YYYY'), null);

INSERT INTO empleado
VALUES ('35311125E', '3200345681', null, 'No', 'Noche', 'Tienda',
TO_DATE ('12-05-1979', 'DD-MM-YYYY'), null);

/* Insertar datos en la tabla ruta_empleado */
INSERT INTO ruta_empleado (nss, estacion, year)
VALUES ('3200123456', 'verano', '2019');

INSERT INTO ruta_empleado (nss, estacion, year)
VALUES ('3200123456', 'verano', '2018');

INSERT INTO ruta_empleado (nss, estacion, year)
VALUES ('3200456789', 'invierno', '2017');

INSERT INTO ruta_empleado (nss, estacion, year)
VALUES ('3200345322', 'primavera', '2019');


/* Insertar datos en la tabla cabinas */
INSERT INTO cabinas (num_cabina, tipo, num_personas, nss, id_reserva)
VALUES (11, 'Suite', 2, '3200123456', 1);

INSERT INTO cabinas
VALUES (243, 'Exterior', 1, '3200234567', 3);

INSERT INTO cabinas
VALUES (443, 'Interior', 2, '3200253486', 4);

/* Insertar datos en la tabla de incidencias */
INSERT INTO incidencia (num_incidencia, fecha, tipo, descripcion, nss)
VALUES (1, TO_DATE('20-04-2018', 'DD-MM-YYYY'), 'Platos Rotos', 'Al camarero se le cayeron los platos', '3200253486');

INSERT INTO incidencia
VALUES (2, TO_DATE('20-01-2019', 'DD-MM-YYYY'), 'Tubería Agujereada', 'Tubería en sala de máquinas pierde líquido', '3200254599');

INSERT INTO incidencia
VALUES (3, TO_DATE('20-02-2019', 'DD-MM-YYYY'), 'Sonar estropeado', 'El sonar no funciona', '3200254599');

/* Insertar datos en la tabla tiendas */
INSERT INTO tienda (nombre, tipo, ubicacion)
VALUES ('Womens Secret', 'regalos', 'proa');

INSERT INTO tienda (nombre, tipo, ubicacion)
VALUES ('Mercadona', 'bebidas', 'popa');

INSERT INTO tienda (nombre, tipo, ubicacion)
VALUES ('Gasco', 'juguetes', 'estribor');

INSERT INTO tienda (nombre, tipo, ubicacion)
VALUES ('Flying Tiger', 'regalos', 'babor');

/* Insertar datos en la tabla empleado_tienda */
INSERT INTO empleado_tienda (nombre, nss, horas_tienda)
VALUES ('Gasco', '3200345681', 4);

INSERT INTO empleado_tienda (nombre, nss, horas_tienda)
VALUES ('Flying Tiger', '3200345681', 4);

/* Insertar datos en la tabla restaurante */
INSERT INTO restaurante (nombre, tipo, menu, precio)
VALUES ('Dolce Vita', 'italiano', 'frutti di mare', 27);

INSERT INTO restaurante 
VALUES ('Sabor a Mexico', 'mexicano', 'mariachi', 22);

INSERT INTO restaurante 
VALUES ('Jumbo', 'asiatico', 'hanyu pinyin', 18);

INSERT INTO restaurante 
VALUES ('Primer Turno', 'principal', 'polbo a feira', 0);

INSERT INTO restaurante 
VALUES ('Segundo Turno', 'principal', 'filloas', 0);

/* Insertar datos en la tabla reserva_restaurante */
INSERT INTO reserva_restaurante (nombre, id_reserva, fecha_hora_res)
VALUES ('Dolce Vita', 1, TO_DATE('29-06-2019 09:30:00 PM', 'DD-MM-YYYY HH:MI:SS PM'));

INSERT INTO reserva_restaurante (nombre, id_reserva, fecha_hora_res)
VALUES ('Dolce Vita', 2, TO_DATE('30-01-2019 10:15:00 PM', 'DD-MM-YYYY HH:MI:SS PM'));

INSERT INTO reserva_restaurante (nombre, id_reserva, fecha_hora_res)
VALUES ('Jumbo', 2, TO_DATE('25-01-2019 09:30:00 PM', 'DD-MM-YYYY HH:MI:SS PM'));

/* Insertar datos en la tabla empleado_restaurante */
INSERT INTO empleado_restaurante (nombre, nss, horas_restaurante)
VALUES ('Dolce Vita','3200253486', 4);

INSERT INTO empleado_restaurante (nombre, nss, horas_restaurante)
VALUES ('Jumbo','3200253486', 4);

/* Insertar datos en la tabla excursion */
INSERT INTO excursion (id_excursion, descripcion, edad_minima, precio)
VALUES (1, 'Excursión turística en el primer puerto de paso', 6, 12);

INSERT INTO excursion
VALUES (2, 'Excursión a las profundidades del mar en mini submarino', 18, 54);

INSERT INTO excursion
VALUES (3, 'Excursión por la zona exterior del barco para mostrar las mejores zonas y de más confort', 3, 8);

/* Insertar datos en la tabla reserva_excursion */
INSERT INTO reserva_excursion (id_excursion, id_reserva, fecha_hora)
VALUES (1, 1, TO_DATE('24-06-2019','dd-MM-yyyy'));

INSERT INTO reserva_excursion (id_excursion, id_reserva, fecha_hora)
VALUES (2, 1, TO_DATE('28-06-2019','dd-MM-yyyy'));

INSERT INTO reserva_excursion (id_excursion, id_reserva, fecha_hora)
VALUES (3, 2, TO_DATE('28-12-2019','dd-MM-yyyy'));

/* Insertar datos en la tabla empleado_excursion */
INSERT INTO empleado_excursion (id_excursion, nss, horas_excursion)
VALUES (1, '3200123456', 3);

INSERT INTO empleado_excursion (id_excursion, nss, horas_excursion)
VALUES (2, '3200254599', 5);

INSERT INTO empleado_excursion (id_excursion, nss, horas_excursion)
VALUES (3, '3200253486', 1);

/* Insertar datos en la tabla actividad */
INSERT INTO actividad (nombre, tipo, edad_minima, descripcion)
VALUES ('StarVegas', 'casino', 18, 'Sala de máquinas tragaperras Vintage');

INSERT INTO actividad
VALUES ('QuickDance', 'baile_latino', 12, 'Bachata para principiantes');

INSERT INTO actividad
VALUES ('Shanti', 'yoga', 16, 'Yoga Iyengar: Nivel debutante');

/* Insertar datos en la tabla reserva_actividad */
INSERT INTO reserva_actividad (nombre, id_reserva, fecha_hora)
VALUES ('QuickDance', 1, TO_DATE('24-06-2019','DD-MM-YYYY'));

INSERT INTO reserva_actividad (nombre, id_reserva, fecha_hora)
VALUES ('Shanti', 3, TO_DATE('28-06-2019','DD-MM-YYYY'));

/* Insertar datos en la tabla empleado_actividad */
INSERT INTO empleado_actividad (nombre, nss, horas_actividad)
VALUES ('Shanti', '3200345322', 8);


/* Para modificar un registro */
-- UPDATE client SET nombre = 'Luis' WHERE dni = '31133465V';

/* Para eliminar un registro */
-- DELETE FROM client WHERE dni = '31133465V';