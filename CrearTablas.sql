/* 
 * CREACIÓN DE TABLAS EN SQL
 * Descripción: ASIX-DAW1B UF2 RA2 CAS6
 * Autores: Vila, Pascual y Masana (Grupo 3)
 * Título: Viajes Montsià
 */
 
/* Tabla para almacenar información sobre clientes */
CREATE TABLE client (
dni varchar(9),
nombre varchar(20) not null,
fecha_nacimiento date not null,
CONSTRAINT client_pk PRIMARY KEY (dni)
);

/* Tabla para almacenar información sobre las rutas */
CREATE TABLE ruta (
estacion varchar(30),
CONSTRAINT ruta_pk PRIMARY KEY (estacion)
);

/* Tabla para almacenar información de las reservas */
CREATE TABLE reserva (
id_reserva int,
precios int not null,
horario_comedor_principal varchar(20),
excursiones_opcionales varchar(50),
actividades_extra varchar(50),
dni varchar(9) not null,
estacion varchar(30) not null,
CONSTRAINT reserva_pk PRIMARY KEY (id_reserva),
CONSTRAINT reserva_client_fk FOREIGN KEY (dni)
REFERENCES client (dni)
ON DELETE CASCADE,
CONSTRAINT reserva_ruta_fk FOREIGN KEY (estacion)
REFERENCES ruta (estacion)
ON DELETE CASCADE,
CONSTRAINT ck_turnos_comedor CHECK (horario_comedor_principal
IN ('19:00', '22:00'))
/* El comedor principal va incluído en el precio solo hay dos turnos para cenar
y no se pueden hacer reservas */
);

/* Añadir Régimen de alojamiento en la tabla reservas */
ALTER TABLE reserva
ADD reg_alojamiento varchar(20) not null
CONSTRAINT ck_alojamiento CHECK (reg_alojamiento
IN ('PC', 'MP', 'SA'));
-- Pensión completa, Media pensión, Solo alojamiento

/* 
 * Tabla para almacenar la relación N:M entre cliente y reserva 
 * (Clientes que suben a bordo del barco)
 */
CREATE TABLE checkin (
dni varchar(9),
id_reserva int,
CONSTRAINT checkin_pk PRIMARY KEY (dni, id_reserva),
CONSTRAINT check_client_fk FOREIGN KEY (dni)
REFERENCES client (dni)
ON DELETE CASCADE,
CONSTRAINT check_reserva_fk FOREIGN KEY (id_reserva)
REFERENCES reserva (id_reserva)
ON DELETE CASCADE
);

/* Tabla para almacenar información de los puertos */
CREATE TABLE puerto (
id_puerto varchar(20),
localidad varchar(30),
CONSTRAINT puerto_pk PRIMARY KEY (id_puerto)
);

/* 
 * Tabla para almacenar atributos de la relacion N:M
 * entre ruta y puerto
 * (Estado de cada barco al pasar por un determinado puerto)
 */
CREATE TABLE lista_puertos (
id_lista int GENERATED ALWAYS AS IDENTITY, -- Sirve para auto-incrementar un valor (Si se añade al insert da error) 
estacion varchar(30),
id_puerto varchar(20),
situacion varchar(20) not null,
CONSTRAINT lista_puertos_pk PRIMARY KEY (id_lista),
CONSTRAINT estacion_fk FOREIGN KEY (estacion)
REFERENCES ruta (estacion)
ON DELETE CASCADE,
CONSTRAINT puerto_fk FOREIGN KEY (id_puerto)
REFERENCES puerto (id_puerto)
ON DELETE CASCADE
);

/* Tabla para almacenar información sobre los empleados */
CREATE TABLE empleado (
dni varchar(10) not null,
nss varchar(50),
nss2 varchar(50),
gerente varchar(30),
horario varchar(30) not null,
lugar_de_trabajo varchar (30) not null,
fecha_contratacion date not null,
fecha_finalizacion date,
CONSTRAINT empleado_pk PRIMARY KEY (nss),
CONSTRAINT ck_gerente CHECK (gerente
IN ('Si','No')),
CONSTRAINT empleado_empleado_fk FOREIGN KEY (nss2)
REFERENCES empleado (nss)
ON DELETE CASCADE,
CONSTRAINT ck_validacion_fecha CHECK
(fecha_finalizacion > fecha_contratacion) -- validación fecha
);

/* 
 * Tabla para almacenar la relación N:M entre ruta y empleado
 * (Rutas que hace cada empleado)
 */
CREATE TABLE ruta_empleado (
id int GENERATED ALWAYS AS IDENTITY, -- Sirve para auto-incrementar un valor (Si se añade al insert da error)
nss varchar(50),
estacion varchar(30),
year varchar(10) not null,
CONSTRAINT ruta_empleado_pk PRIMARY KEY (id),
CONSTRAINT ruta_empleado_fk FOREIGN KEY (estacion)
REFERENCES ruta (estacion),
CONSTRAINT empleado_ruta_fk FOREIGN KEY (nss)
REFERENCES empleado (nss)
);


/* Tabla para almacenar información sobre las cabinas */
CREATE TABLE cabinas (
num_cabina int,
tipo varchar(50) not null,
num_personas int not null,
nss varchar(50),
id_reserva int,
CONSTRAINT cabina_pk PRIMARY KEY (num_cabina),
CONSTRAINT empleado_cabinas_fk FOREIGN KEY (nss)
REFERENCES empleado (nss)
ON DELETE CASCADE,
CONSTRAINT reserva_cabinas_fk FOREIGN KEY (id_reserva)
REFERENCES reserva (id_reserva)
ON DELETE CASCADE,
CONSTRAINT ck_cabina CHECK (tipo
IN ('Suite','Interior', 'Exterior'))
);

/* Tabla para almacenar información sobre las incidencias */
CREATE TABLE incidencia (
num_incidencia int,
fecha date not null,
tipo varchar(50) not null,
descripcion varchar (200) not null,
nss varchar(50) not null,
CONSTRAINT incidencia_pk PRIMARY KEY (num_incidencia),
CONSTRAINT fk_empleado_incidencia FOREIGN KEY (nss)
REFERENCES empleado (nss)
ON DELETE CASCADE
);
-- ALTER TABLE empleado MODIFY nss varchar(50) not null;

/* Tabla para almacenar información sobre las tiendas */
CREATE TABLE tienda (
nombre varchar(30),
tipo varchar(30),
ubicacion varchar(30) not null,
CONSTRAINT tienda_pk PRIMARY KEY (nombre),
CONSTRAINT ck_tienda CHECK (tipo
IN ('regalos','bebidas','juguetes'))
);

/* 
 * Tabla para almacenar atributos de la relación N:M
 * entre empleado y tienda
 * (Horas trabajadas)
 */
CREATE TABLE empleado_tienda (
id int GENERATED ALWAYS AS IDENTITY, -- Sirve para auto-incrementar un valor (Si se añade al insert da error)
nombre varchar(30),
nss varchar(50),
horas_tienda int,
CONSTRAINT empleado_tienda_pk PRIMARY KEY (id),
CONSTRAINT fk_tienda_empleado FOREIGN KEY (nss)
REFERENCES empleado (nss)
ON DELETE CASCADE,
CONSTRAINT fk_empleado_tienda FOREIGN KEY (nombre)
REFERENCES tienda (nombre)
ON DELETE CASCADE
);

/* Tabla para almacenar información sobre los restaurantes */
CREATE TABLE restaurante (
nombre varchar(30),
tipo varchar(20) not null,
menu varchar(20),
precio int not null,
CONSTRAINT restaurante_pk PRIMARY KEY (nombre),
CONSTRAINT ck_restaurante CHECK (tipo
IN ('asiatico','italiano','mexicano', 'principal'))
);

/* 
 * Tabla para almacenar atributos de la relación N:M
 * entre empleado y restaurante
 * (Horas trabajadas)
 */
CREATE TABLE empleado_restaurante (
id int GENERATED ALWAYS AS IDENTITY,
nombre varchar(30),
nss varchar(50),
horas_restaurante int,
CONSTRAINT empleado_restaurante_pk PRIMARY KEY (id),
CONSTRAINT fk_restaurante_empleado FOREIGN KEY (nss)
REFERENCES empleado (nss)
ON DELETE CASCADE,
CONSTRAINT fk_empleado_restaurante FOREIGN KEY (nombre)
REFERENCES restaurante (nombre)
ON DELETE CASCADE
);

/* 
 * Tabla para almacenar atributos de la relación N:M
 * entre reserva y restaurante
 * (Reservas para restaurantes a la carta)
 */
CREATE TABLE reserva_restaurante (
id int GENERATED ALWAYS AS IDENTITY,
nombre varchar(30),
id_reserva int,
fecha_hora_res date not null,
CONSTRAINT reserva_restaurante_pk PRIMARY KEY (id),
CONSTRAINT fk_restaurante_reserva FOREIGN KEY (id_reserva)
REFERENCES reserva (id_reserva)
ON DELETE CASCADE,
CONSTRAINT fk_reserva_restaurante FOREIGN KEY (nombre)
REFERENCES restaurante (nombre)
ON DELETE CASCADE
);

/* Tabla para almacenar información sobre las excursiones */
CREATE TABLE excursion (
id_excursion int,
descripcion varchar(150) not null,
edad_minima int not null,
precio int not null,
CONSTRAINT excursion_pk PRIMARY KEY (id_excursion)
);

/* 
 * Tabla para almacenar atributos de la relación N:M
 * entre empleado y excursión
 * (Horas trabajadas)
 */
CREATE TABLE empleado_excursion (
id int GENERATED ALWAYS AS IDENTITY,
id_excursion int,
nss varchar(50),
horas_excursion int,
CONSTRAINT empleado_excursion_pk PRIMARY KEY (id),
CONSTRAINT fk_excursion_empleado FOREIGN KEY (nss)
REFERENCES empleado (nss)
ON DELETE CASCADE,
CONSTRAINT fk_empleado_excursion FOREIGN KEY (id_excursion)
REFERENCES excursion (id_excursion)
ON DELETE CASCADE
);

/* 
 * Tabla para almacenar atributos de la relación N:M
 * entre reserva y excursión
 * (Lista de Excursiones asignadas a cada reserva)
 */
CREATE TABLE reserva_excursion (
id int GENERATED ALWAYS AS IDENTITY,
id_excursion int,
id_reserva int,
fecha_hora date not null,
CONSTRAINT reserva_excursion_pk PRIMARY KEY (id),
CONSTRAINT fk_excursion_reserva FOREIGN KEY (id_reserva)
REFERENCES reserva (id_reserva)
ON DELETE CASCADE,
CONSTRAINT fk_reserva_excursion FOREIGN KEY (id_excursion)
REFERENCES excursion (id_excursion)
ON DELETE CASCADE
);

/* Tabla para almacenar información sobre las actividades */
CREATE TABLE actividad (
nombre varchar(30),
tipo varchar(30) not null,
edad_minima int not null, -- Añadir al MCD
descripcion varchar (200) not null,
CONSTRAINT actividad_pk PRIMARY KEY (nombre),
CONSTRAINT ck_actividad CHECK (tipo
IN ('baile_latino','yoga','casino')) 
);

/* 
 * Tabla para almacenar atributos de la relación N:M
 * entre empleado y actividad
 * (Horas trabajadas)
 */
CREATE TABLE empleado_actividad (
id int GENERATED ALWAYS AS IDENTITY
(Start With 10 Increment By 10), -- Para Especificar incremento
nombre varchar(30),
nss varchar(50),
horas_actividad int,
CONSTRAINT empleado_actividad_pk PRIMARY KEY (id),
CONSTRAINT fk_actividad_empleado FOREIGN KEY (nss)
REFERENCES empleado (nss)
ON DELETE CASCADE,
CONSTRAINT fk_empleado_actividad FOREIGN KEY (nombre)
REFERENCES actividad (nombre)
ON DELETE CASCADE
);

/* 
 * Tabla para almacenar atributos de la relación N:M
 * entre reserva y actividad
 * (Lista de Actividades asignadas a cada reserva)
 */
CREATE TABLE reserva_actividad (
id int GENERATED ALWAYS AS IDENTITY,
nombre varchar(30),
id_reserva int,
fecha_hora date not null,
CONSTRAINT reserva_actividad_pk PRIMARY KEY (id),
CONSTRAINT fk_actividad_reserva FOREIGN KEY (id_reserva)
REFERENCES reserva (id_reserva)
ON DELETE CASCADE,
CONSTRAINT fk_reserva_actividad FOREIGN KEY (nombre)
REFERENCES actividad (nombre)
ON DELETE CASCADE
);

/* Para eliminar cualquiera de las tablas: */

-- DROP TABLE actividad CASCADE CONSTRAINTS;
-- DROP TABLE cabinas CASCADE CONSTRAINTS;
-- DROP TABLE checkin CASCADE CONSTRAINTS;
-- DROP TABLE client CASCADE CONSTRAINTS;
-- DROP TABLE empleado CASCADE CONSTRAINTS;
-- DROP TABLE empleado_actividad CASCADE CONSTRAINTS;
-- DROP TABLE empleado_excursion CASCADE CONSTRAINTS;
-- DROP TABLE empleado_restaurante CASCADE CONSTRAINTS;
-- DROP TABLE empleado_tienda CASCADE CONSTRAINTS;
-- DROP TABLE excursion CASCADE CONSTRAINTS;
-- DROP TABLE incidencia CASCADE CONSTRAINTS;
-- DROP TABLE lista_puertos CASCADE CONSTRAINTS;
-- DROP TABLE puerto CASCADE CONSTRAINTS;
-- DROP TABLE reserva CASCADE CONSTRAINTS;
-- DROP TABLE reserva_actividad CASCADE CONSTRAINTS;
-- DROP TABLE reserva_excursion CASCADE CONSTRAINTS;
-- DROP TABLE reserva_restaurante CASCADE CONSTRAINTS;
-- DROP TABLE restaurante CASCADE CONSTRAINTS;
-- DROP TABLE ruta CASCADE CONSTRAINTS;
-- DROP TABLE ruta_empleado CASCADE CONSTRAINTS;
-- DROP TABLE tienda CASCADE CONSTRAINTS;
 